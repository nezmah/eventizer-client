import React, { Component } from 'react'
import { Provider } from 'react-redux'
import store from './store'
import { Route } from 'react-router-dom'
import Home from './components/Home'
import SingleEventContainer from './components/events/SingleEventContainer'
import SingleTicketContainer from './components/tickets/SingleTicketContainer'
import CreateTicketContainer from './components/tickets/CreateTicketContainer'
import CreateEventContainer from './components/events/CreateEventContainer'
import RegisterContainer from './components/users/RegisterContainer'

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div>
          <Route
            exact path="/"
            component={Home}
          />

          <Route
            path="/single-event/:id"
            component={SingleEventContainer}
          />

          <Route
            path="/single-ticket/:id"
            component={SingleTicketContainer}
          />

          <Route
            path="/create-ticket"
            component={CreateTicketContainer}
          />

          <Route
            path="/create-event"
            component={CreateEventContainer}
          />
          <Route
            path="/register"
            component={RegisterContainer}
          />
        </div>
      </Provider>
    )
  }
}
