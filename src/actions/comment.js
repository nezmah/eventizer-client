import * as request from 'superagent'

export const CREATE_COMMENT = 'CREATE_COMMENT'

// const baseUrl = 'http://localhost:5000'
const baseUrl = 'https://eventizer-server.herokuapp.com'

function commentCreated(newComment) {
  console.log('IS COMMENT BEING CREATED?')
  return {
    type: CREATE_COMMENT,
    payload: newComment
  }
}

export function createComment(content, author, ticketId, userId) {
  return function (dispatch) {
    // const { jwt } = getState().user.jwt
    request
      .post(`${baseUrl}/comments`)
      // .set('Authorization', `Bearer ${jwt}`)
      .send({ content, author, ticketId, userId })
      .then(res => {
        console.log('RESPONSE', res.body)
        dispatch(commentCreated(res.body))
      }).catch(console.error)
  }
}