import * as request from 'superagent'

export const GET_TICKET = 'GET_TICKET'
export const EDIT_TICKET = 'EDIT_TICKET'

// const baseUrl = 'http://localhost:5000'
const baseUrl = 'https://eventizer-server.herokuapp.com'

function setTicket(currentTicket) {
  return {
    type: GET_TICKET,
    payload: currentTicket
  }
}

export function getTicket(id) {
  console.log('GET TICKET TRIGERING')
  return function (dispatch) {
    request(`${baseUrl}/tickets/${id}`)
      .then(res => {
        console.log('RESPONSE', res.body)
        dispatch(setTicket(res.body))
      }).catch(console.error)
  }
}

function ticketEdited(currentTicket) {
  return {
    type: EDIT_TICKET,
    payload: currentTicket
  }
}

export function editTicket(id, description, picture, price) {
  return function (dispatch, getState) {
    // const { jwt } = getState().user.jwt
    request
      .put(`${baseUrl}/tickets/${id}`)
      // .set('Authorization', `Bearer ${jwt}`)
      .send({ description, picture, price })
      .then(res => {
        console.log('RESPONSE', res.body)
        dispatch(ticketEdited(res.body))
      }).catch(console.error)
  }
}