import request from 'superagent';

export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS'
export const GET_USER = 'GET_USER'

// const baseUrl = 'http://localhost:5000'
const baseUrl = 'https://eventizer-server.herokuapp.com'

function loginSuccess(user) {
  return {
    type: LOGIN_SUCCESS,
    payload: user
  }
}

export function login(username, password) {
  return function (dispatch) {
    request
      .post(`${baseUrl}/login`)
      .send({ username, password })
      .then(res => {
        console.log('LOGIN RESPONSE', res.body)
        dispatch(loginSuccess(res.body))
      })
      .catch(console.error)
  }
}

function registerSuccess(newUser) {
  return {
    type: REGISTER_SUCCESS,
    payload: newUser
  }
}

export function register(username, password, password_confirmation) {
  return function (dispatch) {
    console.log('REGISTER CALLED')
    request
      .post(`${baseUrl}/register`)
      .send({ username, password, password_confirmation })
      .then(res => {
        console.log('REGISTER RESPONSE', res.body)
        dispatch(registerSuccess(res.body))
      })
      .catch(console.error)
  }
}

function setUser(ticketCreator) {
  return {
    type: GET_USER,
    payload: ticketCreator
  }
}

export function getUser(id) {
  console.log('GET USER TRIGERING')
  return function (dispatch) {
    request(`${baseUrl}/users/${id}`)
      .then(res => {
        console.log('RESPONSE', res.body)
        dispatch(setUser(res.body))
      }).catch(console.error)
  }
}