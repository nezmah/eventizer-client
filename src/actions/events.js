import * as request from 'superagent'

export const GET_EVENTS = 'GET_EVENTS'
export const CREATE_EVENT = 'CREATE_EVENT'

// const baseUrl = 'http://localhost:5000'
const baseUrl = 'https://eventizer-server.herokuapp.com'

function setEvents(events) {
  return {
    type: GET_EVENTS,
    payload: events
  }
}

export function getEvents(limit, offset) {
  return function (dispatch) {
    request(`${baseUrl}/events`)
      .query({ limit, offset })
      .then(res => {
        console.log('EVENTS RESPONSE', res.body)
        dispatch(setEvents(res.body))
      }).catch(console.error)
  }
}

function eventCreated(events) {
  return {
    type: CREATE_EVENT,
    payload: events
  }
}

export function createEvent(name, description, picture, price, startDate, endDate) {
  return function (dispatch, getState) {
    // const { jwt } = getState().user.jwt
    request
      .post(`${baseUrl}/events`)
      // .set('Authorization', `Bearer ${jwt}`)
      .send({ name, description, picture, price, startDate, endDate })
      .then(res => {
        console.log('NEW EVENT RESPONSE', res.body)
        dispatch(eventCreated(res.body))
      }).catch(console.error)
  }
}