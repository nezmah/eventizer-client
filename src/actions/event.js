import * as request from 'superagent'

export const GET_EVENT = 'GET_EVENT'

// const baseUrl = 'http://localhost:5000'
const baseUrl = 'https://eventizer-server.herokuapp.com'

function setEvent(event) {
  return {
    type: GET_EVENT,
    payload: event
  }
}

export function getEvent(id) {
  return function (dispatch) {
    request(`${baseUrl}/events/${id}`)
      .then(res => {
        console.log('EVENT RESPONSE', res.body)
        dispatch(setEvent(res.body))
      }).catch(console.error)
  }
}
