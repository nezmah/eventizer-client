import * as request from 'superagent'

export const CREATE_TICKET = 'CREATE_TICKET'

// const baseUrl = 'http://localhost:5000'
const baseUrl = 'https://eventizer-server.herokuapp.com'

function ticketCreated(newTicket) {
  return {
    type: CREATE_TICKET,
    payload: newTicket
  }
}

export function createTicket(author, description, picture, price, eventId, userId) {
  return function (dispatch, getState) {
    // const { jwt } = getState().user.jwt
    request
      .post(`${baseUrl}/tickets`)
      // .set('Authorization', `Bearer ${jwt}`)
      .send({ author, description, picture, price, eventId, userId })
      .then(res => {
        console.log('RESPONSE', res.body)
        dispatch(ticketCreated(res.body))
      }).catch(console.error)
  }
}
