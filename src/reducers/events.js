import { GET_EVENTS, CREATE_EVENT } from "../actions/events";

const initialState = {}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case GET_EVENTS:
      return action.payload
    case CREATE_EVENT:
      return {
        ...action.payload,
        ...state
      }
    default:
      return state
  }
}
