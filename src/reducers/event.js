import { GET_EVENT } from "../actions/event";

export default (state = {}, action = {}) => {
  switch (action.type) {
    case GET_EVENT:
      return action.payload
    default:
      return state
  }
}
