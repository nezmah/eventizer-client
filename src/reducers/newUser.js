import { REGISTER_SUCCESS } from '../actions/user'

export default function (state = {}, action) {
  switch (action.type) {
    case REGISTER_SUCCESS:
      return action.payload
    default:
      return state
  }
}