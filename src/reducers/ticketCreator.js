import { GET_USER } from "../actions/user"

export default (state = {}, action = {}) => {
  switch (action.type) {
    case GET_USER:
      return action.payload
    default:
      return state
  }
}