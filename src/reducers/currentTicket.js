import { GET_TICKET, EDIT_TICKET } from "../actions/currentTicket"

export default (state = {}, action = {}) => {
  switch (action.type) {
    case GET_TICKET:
      return action.payload
    case EDIT_TICKET:
      return action.payload
    default:
      return state
  }
}