import { combineReducers } from 'redux'
import events from './events'
import event from './event'
import user from './user'
import newUser from './newUser'
import newTicket from './newTicket'
import newComment from './newComment'
import currentTicket from './currentTicket'
import ticketCreator from './ticketCreator'

export default combineReducers({
  events,
  event,
  user,
  newUser,
  newTicket,
  newComment,
  currentTicket,
  ticketCreator
})