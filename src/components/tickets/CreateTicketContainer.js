import React, { Component } from 'react'
import { connect } from 'react-redux'
import CreateTicket from './CreateTicket'
import { createTicket } from '../../actions/ticket'

class CreateTicketContainer extends Component {
  state = {
    created: false,
    message: "Ticket created successfully :)"
  }

  onSubmit = (event) => {
    const { currentEvent, author, userId } = this.props
    console.log('EVENT ID', currentEvent.id)
    const { description, picture, price } = this.state
    event.preventDefault()
    this.props.createTicket(
      author,
      description,
      picture,
      price,
      currentEvent.id,
      userId
    )

    this.setState({
      description: "",
      picture: "",
      price: "",
      created: !this.state.created
    })
    const successMessage = () => {
      setTimeout(() =>
        this.props.history.push('/')
        , 2000)
    }
    successMessage()
  }

  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    return (
      <div>
        <CreateTicket
          onSubmit={this.onSubmit}
          onChange={this.onChange}
          state={this.state}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentEvent: state.event,
    author: state.user.name,
    userId: state.user.id
  }
}

const mapDispatchToProps = {
  createTicket
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateTicketContainer)