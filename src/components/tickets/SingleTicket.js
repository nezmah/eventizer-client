import React, { Component } from 'react'
import { connect } from 'react-redux'
import Comment from '../commens/Comment'
import { getTicket } from '../../actions/currentTicket'
import { createComment } from '../../actions/comment'
import { getUser } from '../../actions/user'
import EditTicketContainer from './EditTicketContainer'
import './SingleTicket.css'
import Header from '../Header'

class SingleTicket extends Component {

  state = {
    content: "",
    posted: false,
    message: "Comment posted sucessfully, it will be visible next time you visit this page :)",
    risk: 5,
  }

  async componentDidMount() {
    const { currentTicket } = this.props
    this.props.getTicket(this.props.id)
    this.props.getUser(currentTicket.userId)
    this.setState({
      risk: this.riskCalc
    })
    setTimeout(() => this.setState({
      risk: this.riskCalc()
    }), 500)
  }

  onSubmit = (event) => {
    const { content } = this.state
    const { username, ticketId, userId } = this.props
    event.preventDefault()
    this.props.createComment(content, username, ticketId, userId)
    this.setState({
      content: "",
      posted: !this.state.posted
    })
    const successMessage = () => {
      setTimeout(() => this.setState({
        message: ""
      }), 1500)
    }
    successMessage()

  }
  riskCalc = () => {
    const { usersTickets, eventTickets, price, currentTicket } = this.props
    const slicedTime = currentTicket.createdAt.slice(11, 13)
    const time = parseInt(slicedTime)
    let risk = 5

    if (usersTickets === 1) { risk += 10 }
    let prices = eventTickets.map(ticket => {
      return ticket.price
    })

    const averagePrice = prices.reduce((acc, curr) => acc + curr, 0) / prices.length

    const diffMinor = (((averagePrice - price) / averagePrice) * 100).toFixed(2)
    if (diffMinor > 0 && diffMinor) { risk += diffMinor }

    const diffMajor = (((averagePrice - price) / price) * 100).toFixed(2)
    if (diffMajor > 0 && diffMajor <= 10) { risk -= diffMajor }
    if (diffMajor > 10) { risk -= 10 }

    if (time >= 9 && time < 18) { risk += 10 }
    if (time < 9 && time >= 18) { risk -= 10 }

    if (currentTicket.comments.length > 3) { risk += 5 }

    if (risk < 5) { return 5 }
    else if (risk > 95) { return 95 }
    else return risk
  }

  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    const { description, price, picture, eventName, author, currentTicket, username } = this.props
    return (
      <div>
        {this.props.showDetails && <Header />}
        <div className='ticket-card'>
          {this.props.showDetails && <h2 className="ticket-title">Ticket for {eventName}</h2>}
          <img className="ticket-img" src={picture} alt="" />
          <p> {description}</p>
          {this.props.showDetails &&
            <div>
              <p>Ticket created by {author}</p>
              <p>risk: {this.state.risk} %</p>
              <p>price: {price} €</p>
              {currentTicket.author === username && <EditTicketContainer />}

              {this.props.userId &&
                <div><h4>Post a comment:</h4>
                  <Comment
                    onSubmit={this.onSubmit}
                    onChange={this.onChange}
                    state={this.state}
                  />
                </div>}
              {this.state.posted && this.state.message}
              <br />
              <p>Comments:</p>
              {this.props.currentTicket.comments && this.props.currentTicket.comments.map(comment => {
                return <div key={comment.id}>
                  <h4> {comment.author}: </h4>
                  <i>"{comment.content}"</i>
                </div>
              })}
            </div>
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentTicket: state.currentTicket,
    userId: state.user.id,
    username: state.user.name,
    ticketId: state.currentTicket.id,
    eventName: state.event.name,
    eventTickets: state.event.tickets,
    usersTickets: state.ticketCreator.tickets
  }
}

const mapDispatchToProps = {
  getTicket,
  createComment,
  getUser
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleTicket)