import React, { Component } from 'react'

export default class EdiTticket extends Component {
  render() {
    const { onChange, onSubmit, state } = this.props
    return (
      <div>
        <h3>Be free to change data you entered when creating the ticket</h3>
        <form onSubmit={onSubmit}>
          <input
            type="text"
            name="description"
            placeholder="Description"
            value={state.description}
            onChange={onChange}
          />
          <br />
          <input
            type="text"
            name="price"
            placeholder="Price"
            value={state.price}
            onChange={onChange}
          />
          <br />
          <input
            type="text"
            name="picture"
            placeholder="Image url (http://image.jpeg)"
            value={state.picture}
            onChange={onChange}
          />
          <br />
          <button type="submit">Save changes</button>
        </form>
        {state.submitted && state.editMessage}
      </div>
    )
  }
}
