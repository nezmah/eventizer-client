import React, { Component } from 'react'
import { connect } from 'react-redux'
import SingleTicket from './SingleTicket'

import { getEvents } from '../../actions/events'
import { getTicket } from '../../actions/currentTicket'

class SingleTicketContainer extends Component {
  state = {
    details: false
  }

  onSubmit = () => {
    console.log('AM I CLICKING?? ')
    this.props.getTicket(this.props.match.params.id)
  }

  showDetails = () => {
    this.setState({
      details: !this.state.details
    })
  }

  render() {
    console.log('sINGLEtICKETcONTANER PROPS', this.props)
    const { currentTicket } = this.props
    return (
      this.props.tickets ?
        <div>

          <div>
            <SingleTicket
              id={this.props.match.params.id}
              author={currentTicket.author}
              description={currentTicket.description}
              price={currentTicket.price}
              picture={currentTicket.picture}
              showDetails={this.showDetails}
              onClick={this.onSubmit}
            />
          </div>
        </div>
        : "Loading..."
    )
  }
}

const mapStateToProps = (state) => {
  return {
    event: state.event,
    tickets: state.event.tickets,
    currentTicket: state.currentTicket
  }
}
const mapDispatchToProps = { getEvents, getTicket }

export default connect(mapStateToProps, mapDispatchToProps)(SingleTicketContainer)