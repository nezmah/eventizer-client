import React, { Component } from 'react'

export default class CreateTicket extends Component {
  render() {
    const { onChange, onSubmit, state } = this.props
    return (
      <div>
        <h3>Fill in all required fields to create a new ticket</h3>
        <form onSubmit={onSubmit}>
          <input
            type="text"
            name="description"
            placeholder="Description"
            value={state.description}
            onChange={onChange}
          />
          <input
            type="text"
            name="price"
            placeholder="Price"
            value={state.price}
            onChange={onChange}
          />
          <input
            type="text"
            name="picture"
            placeholder="Image url (http://image.jpeg)"
            value={state.picture}
            onChange={onChange}
          />
          <button type="submit">Save</button>
        </form>
      </div>
    )
  }
}
