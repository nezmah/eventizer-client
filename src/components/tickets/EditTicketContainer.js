import React, { Component } from 'react'
import { connect } from 'react-redux'
import EditTicket from './EditTicket'
import { editTicket } from '../../actions/currentTicket'

class EditTicketContainer extends Component {
  state = {
    submitted: false,
    editMessage: "Your changes have been saved :)"
  }

  onSubmit = (event) => {
    const { currentTicket } = this.props
    const { description, picture, price } = this.state
    event.preventDefault()
    this.props.editTicket(
      currentTicket.id,
      description,
      picture,
      price,
    )

    this.setState({
      description: "",
      picture: "",
      price: "",
      submitted: !this.state.submitted
    })
    const successMessage = () => {
      setTimeout(() => this.setState({
        editMessage: ""
      }), 1500)
    }
    successMessage()
  }

  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    console.log('STATE OF edit-ticekt', this.state)
    return (
      <div>
        <EditTicket
          onSubmit={this.onSubmit}
          onChange={this.onChange}
          state={this.state}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    currentTicket: state.currentTicket
  }
}

const mapDispatchToProps = {
  editTicket
}

export default connect(mapStateToProps, mapDispatchToProps)(EditTicketContainer)