import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import SingleEvent from './events/SingleEvent'
import Header from './Header'
import { getEvents } from '../actions/events'
import './Home.css'

class Home extends Component {
  state = {
    details: false
  }
  componentDidMount() {
    this.props.getEvents()
  }

  callNext = () => {
    this.props.getEvents(9, 10)
  }

  callPrev = () => {
    this.props.getEvents(9)
  }

  render() {
    console.log('HOME PROPS', this.props)
    return (
      this.props.events.events ?
        <div className='home-container'>
          <Header />
          <br />
          {this.props.logged && <Link to="/create-event"><button className='event-btn'>Create a new Event</button></Link>}

          <h3 className="main-title">Upcoming events</h3>
          {this.props.events && this.props.events.events.map(event => {
            return <Link
              key={event.id}
              to={`/single-event/${event.id}`}
            >
              <SingleEvent
                name={event.name}
                description={event.description}
                picture={event.picture}
                startDate={event.start_date}
                endDate={event.end_date}
                state={this.state}
              />
            </Link>
          })}

          <div className='pagina-div'>
            <button className='pagina' onClick={this.callPrev}>Prev </button>
            <button className='pagina' onClick={this.callNext}>Next </button>
          </div>
        </div> : <h3>loading...</h3>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    events: state.events,
    logged: state.user.id
  }
}

const mapDispatchToProps = {
  getEvents,
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)