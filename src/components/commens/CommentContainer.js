import React, { Component } from 'react'
import { connect } from 'react-redux'
import Comment from './Comment'
import { createComment } from '../../actions/comment'

class CommentContainer extends Component {
  state = {
    content: ""
  }

  onSubmit = (event) => {
    const { content } = this.state
    const { username, ticketId, userId } = this.props
    event.preventDefault()
    this.props.createComment(content, username, ticketId, userId)
    this.setState({ content: "" })
  }

  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    return (
      <div>
        <Comment
          onSubmit={this.onSubmit}
          onChange={this.onChange}
          state={this.state}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userId: state.user.id,
    username: state.user.name,
    ticketId: state.currentTicket.id
  }
}

const mapDispatchToProps = {
  createComment
}

export default connect(mapStateToProps, mapDispatchToProps)(CommentContainer)