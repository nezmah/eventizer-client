import React, { Component } from 'react'

export default class Comment extends Component {
  render() {
    const { onChange, onSubmit, state } = this.props
    return (
      <div>
        <form onSubmit={onSubmit}>
          <textarea
            name="content"
            placeholder="Your comment..."
            value={state.content}
            onChange={onChange}
          />
          <button type="submit">Post</button>
        </form>
      </div>
    )
  }
}
