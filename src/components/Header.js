import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import LoginFormContainer from './users/LoginFormContainer'
import './Header.css'

export default class Header extends Component {

  render() {
    return (
      <div className='event-header'>
        <Link to='/'><h2>Eventizer</h2></Link>
        <Link to='/'><h5>home of greate events</h5></Link>
        <LoginFormContainer />
        <Link to="/register">
          <button className='btn register-btn'>
            Register
        </button>
        </Link>
      </div>
    )
  }
}
