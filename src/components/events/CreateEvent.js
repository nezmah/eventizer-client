import React, { Component } from 'react'
import './CreateEvent.css'

export default class CreateEvent extends Component {
  render() {
    const { onChange, onSubmit, state } = this.props
    return (
      <div className='event-form'>
        <h3 className='new-event-message'>Fill in all the required fields to create a new Event</h3>
        <form onSubmit={onSubmit}>
          <input
            className='input-name'
            type="text"
            name="name"
            placeholder="Name"
            value={state.name}
            onChange={onChange}
          />
          <br />
          <input
            className='input-text'
            type="text"
            name="description"
            placeholder="Description"
            value={state.description}
            onChange={onChange}
          />
          <br />
          <textarea
            rows="4"
            cols="50"
            className='textarea-pic'
            type="text"
            name="picture"
            placeholder="Image url (http://image.jpeg)"
            value={state.picture}
            onChange={onChange}
          />
          <br />
          <label htmlFor="startDate">Starts on:</label>
          <br />
          <input
            type="date"
            name="startDate"
            placeholder="YYYY-MM-DD"
            value={state.startDate}
            onChange={onChange}
          />
          <br />
          <label htmlFor="endDate">Finishes:</label>
          <br />
          <input
            type="text"
            name="endDate"
            placeholder="YYYY-MM-DD"
            value={state.endDate}
            onChange={onChange}
          />
          <br />
          <button className='eventSave-btn' type="submit">Save</button>
        </form>
      </div>
    )
  }
}
