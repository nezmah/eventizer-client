import React, { Component } from 'react'
import './SingleEvent.css'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'

import SingleTicket from '../tickets/SingleTicket'
import { getEvent } from '../../actions/event'
import Header from '../Header'

class SingleEvent extends Component {
  state = {
    description: "",
    pricture: "",
    created: false
  }
  componentDidMount() {
    this.props.getEvent(this.props.id)
  }

  render() {
    return (
      <div>
        {this.props.showDetails && <Header className='single-header' />}
        <div className='card'>
          <img src={this.props.picture} alt="" />
          <p className='title'>{this.props.name}</p>
          <p>{this.props.description}</p>
          <p>Startring on: {this.props.startDate}</p>
          <p>Finishing: {this.props.endDate ? this.props.endDate : this.props.startDate}</p>

          {this.props.showDetails ?
            <div>
              {this.props.userId && <div> <Link to='/create-ticket'>
                <button className='ticket-btn'>Create new Ticket for this event</button>
              </Link></div>}
              Tickets:
                  {this.props.tickets.map(ticket => {
                return <Link
                  key={ticket.id}
                  to={`/single-ticket/${ticket.id}`}
                >
                  <SingleTicket
                    id={ticket.id}
                    description={ticket.description}
                    price={ticket.price}
                    picture={ticket.picture}
                    author={ticket.author}
                    state={this.state}
                  />
                </Link>
              })}

            </div>
            : ""}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    events: state.events,
    newTicket: state.newTicket,
    author: state.user.name,
    userId: state.user.id
  }
}

const mapDispatchToProps = {
  getEvent
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleEvent)
