import React, { Component } from 'react'
import { connect } from 'react-redux'
import CreateEvent from './CreateEvent'
import { createEvent } from '../../actions/events'

class CreateEventContainer extends Component {
  state = {
    created: false,
    message: "Event created successfully :)"
  }

  onSubmit = (event) => {
    const { name, description, picture, startDate, EndDate } = this.state
    event.preventDefault()
    this.props.createEvent(
      name,
      description,
      picture,
      startDate,
      EndDate
    )

    this.setState({
      name: "",
      description: "",
      picture: "",
      startDate: "",
      endDate: "",
      created: !this.state.posted
    })
    const successMessage = () => {
      setTimeout(() =>
        this.props.history.push('/')
        , 2000)
    }
    successMessage()
  }

  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    return (
      <div>
        <CreateEvent
          onSubmit={this.onSubmit}
          onChange={this.onChange}
          state={this.state}
        />
        {this.state.created && this.state.message}
      </div>
    )
  }
}

const mapDispatchToProps = {
  createEvent
}

export default connect(null, mapDispatchToProps)(CreateEventContainer)