import React, { Component } from 'react'
import { connect } from 'react-redux'

import SingleEvent from './SingleEvent'
import './SingleEventContainer.css'

import { getEvents } from '../../actions/events'

class SingleEventContainer extends Component {
  state = {
    details: false
  }

  showDetails = () => {
    this.setState({
      details: !this.state.details
    })
  }

  render() {
    const { events } = this.props
    const event =events.events && events.events.find(event => {
      return event.id == this.props.match.params.id
    })
    return (
      event ?
        <div>
          <SingleEvent
            id={event.id}
            name={event.name}
            description={event.description}
            picture={event.picture}
            startDate={event.start_date}
            endDate={event.end_date}
            tickets={event.tickets}
            showDetails={this.showDetails}
          />
        </div>
        : "Loading..."
    )
  }
}

const mapStateToProps = (state) => {
  return {
    events: state.events
  }
}
const mapDispatchToProps = { getEvents }

export default connect(mapStateToProps, mapDispatchToProps)(SingleEventContainer)