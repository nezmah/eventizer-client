import React, { Component } from 'react'
import './LoginForm.css'

export default class LoginForm extends Component {

  render() {
    const { onChange, onSubmit, state } = this.props
    return (
      <div>
        <form className='login-form'
          onSubmit={onSubmit}>
          <input
            className='loginUser-input'
            type='text'
            name='username'
            placeholder='Username'
            onChange={onChange}
            value={state.username}
          />
          <input
            type='password'
            name='password'
            placeholder='Password'
            onChange={onChange}
            value={state.password}
          />
          <button className='login-btn' type="submit">Login</button>
        </form>
      </div>
    )
  }
}
