import React, { Component } from 'react'

export default class Register extends Component {
  render() {
    const { onChange, onSubmit, state } = this.props
    return (
      <div>
        <form onSubmit={onSubmit}>
          <input
            type='text'
            name='username'
            placeholder='Username'
            onChange={onChange}
            value={state.username}
          />
          <input
            type='password'
            name='password'
            placeholder='Password'
            onChange={onChange}
            value={state.password}
          />
          <input
            type='password'
            name='password_confirmation'
            placeholder='Confirm you password'
            onChange={onChange}
            value={state.password_confirmation}
          />
          <button type="submit">Register</button>
        </form>
      </div>
    )
  }
}
