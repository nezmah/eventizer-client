import React, { Component } from 'react'
import { connect } from 'react-redux'
import LoginForm from './LoginForm'

import { login } from '../../actions/user'

class LoginFormContainer extends Component {
  state = {
    username: "",
    password: "",
    logged: false,
    message: "Logged in succesfully"
  }

  onSubmit = (event) => {
    const { username, password } = this.state
    event.preventDefault()
    this.props.login(username, password)
    this.setState({
      username: "",
      password: "",
      logged: !this.state.logged
    })
    const successMessage = () => {
      setTimeout(() => this.setState({
        message: ""
      }), 1500)
    }
    successMessage()
  }

  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    return (
      <div>
        <LoginForm
          onSubmit={this.onSubmit}
          onChange={this.onChange}
          state={this.state}
        />
        {this.state.logged && this.state.message}
      </div>
    )
  }
}
const mapDispatchToProps = {
  login
}

export default connect(null, mapDispatchToProps)(LoginFormContainer)