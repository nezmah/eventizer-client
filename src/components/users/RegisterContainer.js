import React, { Component } from 'react'
import { connect } from 'react-redux'
import Register from './Register'
import { register } from '../../actions/user'
import { Link } from 'react-router-dom'

class RegisterContainer extends Component {
  state = {
    username: "",
    password: "",
    password_confirmation: "",
    registered: false,
    message: "Registered successfully"
  }

  onSubmit = (event) => {
    const { username, password, password_confirmation } = this.state
    event.preventDefault()
    this.props.register(username, password, password_confirmation)
    this.setState({
      username: "",
      password: "",
      password_confirmation: "",
      registered: !this.state.registered
    })
    const successMessage = () => {
      setTimeout(() => this.setState({
        message: ""
      }), 1500)
    }
    successMessage()
  }

  onChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  render() {
    console.log('REGISTER PROPS', this.props)
    return (
      <div>
        <Register
          onSubmit={this.onSubmit}
          onChange={this.onChange}
          state={this.state}
        />
        {this.state.registered && this.state.message}

        <Link to="/"><button>Home</button></Link>
      </div>
    )
  }
}

const mapDispatchToProps = {
  register
}

export default connect(null, mapDispatchToProps)(RegisterContainer)