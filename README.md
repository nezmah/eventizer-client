# Eventizer-client

see deployed app on [Heroku](https://eventizer-client.herokuapp.com)

## What this project is about

This is a client side of a 'Ticket selling' app written in JavaScript using React and Redux.
It was a final assignment for graduating [the Codaisseuer Academy](https://codaisseur.com/become-a-developer).
Check out the server-side of this project [here](https://bitbucket.org/nezmah/eventizer-server/src/master/)

Visitor is able to see all ongoing events and tickets for those events.  
Visitor needs to register in order to do more than just watching the content of the site.  
Only registered user can be logged in.
Logged user can  
- create a new event  
- create a ticket for an event  
- comment on tickets  
- edit name, price and description of ticket that s/he created


## Table of contents: 

- **[Technologies used](#technologies-used)**
- **[Goals for this project](#goals-for-this-project)**
- **[How to install](#how-to-install)**
- **[Features built so far](#features-built-so-far)**
- **[Routes used in this app](#routes-used-in-this-app)**
- **[Features to add in the future](#features-to-add-in-the-future)**
- **[Create-react-app-docs](#create-react-app)**

## Technologies used

#### 👇 Click links to view some samples in this project

- **[React](./src/components/Home.js)**
- **[Redux](./src/reducers/events.js)**
- **[Redux-thunk](./src/actions/currentTicket.js)**
- **[CSS](./src/components/tickets/SingleTicket.css)**

## Goals for this project: 
- To prove that I am ready for work on real world project
- To combine front-end and backend apps in one working project
- To practice disciplined git usage
- To give best of myself in short time and create fully working app rich in features

## How to install
- clone the [server repo](https://nezmah@bitbucket.org/nezmah/eventizer-server/src/master/). 
- run the postgres db on port 1234 with username 'postres' and password 'secret'
- `npm install`
- `npm start`

- clone this repo
- open new terminal 
- `npm install`
- `npm start`
- it should open your browser on port localhost:3000 with the working app :)

## Features built so far 
- **[Create Routing with React-router ](./src/App.js)**
- **[Create Events listing](./src/components/Home.js)**
- **[Create snigle Event view](./src/components/events/SingleEventContainer.js)**
- **[Create form for listing a new Event](./src/components/events/CreateEventContainer.js)**
- **[Create single ticket view](./src/components/tickets/SingleTicketContainer.js)**
- **[Create form for listing a new Ticket for single Event](./src/components/tickets/CreateTicketContainer.js)**
- **[Create form for editing a single Ticket](./src/components/tickets/editTicketContainer.js)**
- **[Create a Comment for a specific Ticket](./src/components/comments/CommentContainer.js)**
- **[Create Register Form](./src/components/users/RegisterContainer.js)**
- **[Create Login Form](./src/components/users/LoginFormContainer.js)**
- **[Create Risk algorithm that calculates possibility of ticked being counterfitted based on numer of tickets user created, number of comments that ticket have, difference of average price of all tickets for specifit event and time of ticket creation](./src/components/users/SingleTicket.js)**
- **[Add some styling](./src/components/tickets/SingleTicket.css)**
- **[Add pagination](./src/components/Home.js)**

## Routes used in this app
- **[root - home](./src/components/Home.js)**
    Listing of all Advertisments

- **[/single-event/:id](./src/components/events/SingleEventContainer.js)**
    Single Event listing

- **[/create-event](./src/components/events/CreateEventContainer.js)**
    Create a new Event

- **[/single-ticket/:id](./src/components/tickets/SingleTicketContainer.js)**
    Single Ticket listing

- **[/create-ticket](./src/components/tickets/CreateTicketContainer.js)**
    Create a new Ticket

- **[/register](./src/components/users/RegisterContainer.js)**
    Form for registering a new User

## Features to add in the future
- Improve pagination
- Improve risk calculation
- Improve Authentication
- Improve styling

## Create React App

This project was bootstrapped with create-react-app cli.

**[The standard create-react-app docs can be found in here](./create-react-app-docs.md)**

